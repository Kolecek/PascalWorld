<h1>Knihovna Lite a uk�zkov� program World</h1>
<p class="nastred">autorem JIVA a dal?� z internetu</p>

<p>C�lem knihovny Lite je poskytnout rozhran�, jeden celek, nadstavbu nad samotn�m Borland Pascalem 7.0, se kter�m lze jednodu�e a rychle seskl�dat programy, kter� budou v grafick�m re�imu a umo�n� p�ehr�vat i zvuky. Nap��klad rychle napsat hru, kter� vypad� naprosto seri�zn�. Nebo jinak: mohl by to b�t program d�laj�c� cokoliv jin�ho, ov�em chceme vyn�st na obrazovku nap��klad graf nebo informace. C�lem tohoto jednotn�ho celku "na v�echno" je, aby si zachoval status "Lite", tzn. odleh�en�, mal� nebo chcete-li bez cukru.</p>

<p>V sou�asn� dob� je Lite koncipovan� na program pro re�ln� re�imu procesoru. Z voln�ch 640 KB pam�ti obsad� asi polovinu, mo�n� i v�c. Z toho vypl�v�, �e v�sledn� program Litu m��e nab�t asi tak velikosti 128..192 KB. Pracuje se na verzi pro protected mode, ta bude vyu��vat RTM.EXE. Lite se jako projekt pro re�ln� re�im a sou��st v�sledn�ho programu logicky skl�d� z men��ch knihoven. Umo�n� program 150 KB v grafice, se zvuky atd. bez nutnosti dodat jakoukoli dal�� knihovnu. Jedn� se o programovac� sadu, kter� vznikla sestaven�m z knihoven r�zn�ch autor�. Dal��mi zdroji byly manu�lov� programy jako SYSMAN, PCGPE nebo ATHELP. Knihovna je tedy sb�raniko, je ov�em tak zaform�tovan�, �e by m�lo d�vat smysl.</p>


<h3>Podknihovna *1* - GLOB</h3>

<p>V t�to ��sti se nach�zej� z�kladn� deklarace popisuj�c� pam�ov� model programu (v tuto chv�li real). V souboru Glob, kter� p�edstavuje prvn� podknihovnu tpu, najdeme seznam v�ech �estn�cti ��st�. Ka�d� ��st je podknihovna, ve fin�le se linkuj� do jednotn�ho Litu, kter� p�esahuje 64 KB. V globu je to rozd�len� na v�eobecn� Types, Variables, Const a ka�d� z t�chto ��st� je d�l rozd�len� na 16 pod��st�. V boku je nadeps�n prefix, jm�na podknihovny, v druh�m boku je p��padn� koment��, v t�le asi veprost�ed jsou definovan� typy, prom�nn� a konstanty. 16 - 16 - 16. I v p��pad�, �e dan� sekce neobsahuje nic, p�esto je zad�na jako kolonka pro pozd�j�� mo�n� dopln�n�.</p>

<p>N�e n�sleduje ��st, kde zase v 16 sekc�ch podknihoven najdeme programov� mo�nosti. Ty si prohl�dn�te, to jsou stavebn� kameny pro v�sledn� program. Ty a jedin� ty. Teoreticky k nim pat�� samoz�ejm� je�t� programov� prvky pascalu sam�ho jaksi bez knihoven. Od toho je to Lite, �e je to stroh�. M�l by v tom b�t p�ehled. C�lem je v�dy minimalizace okoln�ch dopl�uj�c�ch variabilit. Co je mo�n�, je dan� staticky. To vede ke rychl�mu ovl�dnut� rozhran� Lite, prakticky bez nutnosti na�erpat obs�hl� t�ma k rychl� produktivit� grafick�ch program�.</p>

<p>Za Interfacem je Implementation, kter� se bez v�jimek zakl�d� na include souborech jako v C��ku. Include soubory naleznete ve slo�ce STORE v podslo�k�ch s ��sly a zkr�cen�mi jm�ny. Sna�il jsem se i o to, aby file browser Borland Pascalu, kter� zobrazuje dva sloupce, byl optim�ln� pokryt 8+8 soubory podknihoven a d�le 16 slo�kami ve store. Jednotliv� koncov� include funkce by m�ly b�t jen tak po�etn�, aby si file browser zachoval schopnost rychl�ho nalezen� a otev�en� souboru. Kompilace prob�h� bat souborem, kter� m� p�elo�it v�echny podknihovny najednou, ale pokud chcete, kompilujte manu�ln�, postupn� od souboru GLOB k dal��m podknihovn�m. V�echny jsou na sob� z�visl� navz�jem se stoupaj�c� tendenc� pot�eby v�ech v�emi.</p>


<h3>Podknihovna *2* - RAM</h3>

<p>V t�to podknihovn� se sna��m o obsluhu vy��� pam�ti. Dokud je knihovna pro real, realizuji to pomoc� XMS. Poprv� je vid�t, �e p��kazy maj� tak� prefixy. Je to pro p�ehled v knihovn�, aby bylo z�ejm�, �e je pou�it stavebn� prvek Litu.</p>


<h3>Podknihovna *3* - DOS</h3>

<p>Tato pas� je zam��len� n�hrada DOSU a CRT, kter� ale nen� dota�en� do konce, je jen v n�znaku nejnutn�j��ch funkc�. K �emu to je, kdy� je DOS a CRT v z�kladn�m pascalu. �prava na bez CRT a bez DOSu povede k odstran�n� probl�m� spojen�ch s chybou v n�kter�ch verz�ch jednotky CRT. Knihovna Lite by vypadala l�pe, kdyby nevy�adovala nic, dokonce ani DOS a CRT. Nedokon�eno.</p>


<h3>Podknihovna *4* - VGA</h3>

<p>Lite pracuje s VGA fixn� v re�imu 320x200x256. V GLOBu je p�ipraven� pam�ov� prostor Buffers. Definoval jsem 4 Buffery po 64 KB o��slovan� 0 1 2 3. Jsou to pointery, kter� ukazuj� na �ty�i bloky 64 KB. Ty jsou p��tomn� v pam�ti po celou dobu b�hu programu. V�echny se pou�ij�. Buffer ��slo 4 pak p�edstavuje VIDEO RAM na segmentu A000h. Pr�ce s VGA je pojata takto: Buffer 0 slou�� jako pomocn� pro na��t�n� ze soubor�. Do Buffer� 1 a 2 se nahr�vaj� kousky grafiky a grafika fontu a pak se procesem p�enesen� obd�ln� oblasti kop�ruj� d�le do Bufferu 3. Ten p�edstavuje fin�ln� framebuffer, kter� je cyklicky vys�l�n (flip) na monitor (Buffer 4).</p>

<p>Podknihovna um� nakreslit pixel, ��ru a pr�zdn� nebo pln� obd�ln�k, vymazat nebo zkop�rovat cel� buffer (v�etn� synchronizace s obrazovkou - WaitRetrace) a r�zn�mi zp�soby p�en�et obd�ln�ky obsahu z Bufferu do Bufferu (cel� nebo s vynech�n�m pr�hledn�ch pixel�).</p>


<h3>Podknihovna *5* - SVGA</h3>

<p>Stejn� jako VGA pracuje s fixn�m rozli�en�m 640x480, 256 barev. Nen� o moc pomalej�� ne� VGA, tzn. pr�tok dat odpov�d� takov� rychlosti, �e to sta�� na v�t�inu �loh a nikde nic neblik�, ov�em vzhledem k v�ce ne� �ty�n�sobn�mu objemu grafick�ch dat se dost�v�me na rychlost pouze 25 FPS a m�n�. Pou��vaj� se stejn� Buffery 0123 jako ve VGA, jsou v programu platn� v�ude jako vymezen� prostor. Buffer 4 u� kv�li bankov�n� VRAM nem� smysl. M�me mo�nost ps�t p��mo na viditelnou sc�nu, nebo pou��t framebuffer v pam�ti karty (TogglePaging). Jm�na a parametry procedur by m�ly b�t stejn� jako u VGA (Pixel, Rectangle, Bar, Spr, BarZ, SprZ, Clear, Flip, WaitRetrace), jen se li�� prefixem SVGA u p��kaz�.</p>


<h3>Podknihovna *6* - 3DFX</h3>

<p>Rusk� knihovna Inertia, kterou jsem za�lenil do Litu tak, aby se p�ehledn� zkompilovala ve verzi pro re�ln� re�im a stala se sou��st� mo�nost�. V p��slu�n� ��sti STORE (slo�ka 3DFX) se nach�zej� soubory knihovny Inertia = profesion�ln� 3D knihovna, kter� se pou��vala v dob� ran�ch grafick�ch akceler�tor� a pak byla uve�ejn�na zdarma. Je pom�rn� obs�hl�. Umo��uje softwarov� renderovat 3D objekty (form�t GVO) na virtu�ln� obrazovku do velikosti 64 KB (tedy 320x200 px).</p>


<h3>Podknihovna *7* - TEXT</h3>

<p>Obsahuje t�i druhy grafick�ho vypisov�n� textu a ��sel. Prvn� OutText p�e �esk�m podom�cku sestaven�m fontem (const). Um� b�n� znaky, ale se specifi�t�j��m znakem si neporad�. Druh� WriteText p�e barevn�m u�ivatelsk�m fontem: do Bufferu 1 nebo 2 se nahraje tabulka znak� (p��kaz load Default font) a WriteText z n� potom bere jednotliv� p�smena a p�en�� je na v�stup. Je to k tomu, abyste si mohli podle vzorov� tabulky sestavit svou vlastn�, t�eba jako BMP. Zm�n� se jen zdrojov� soubor a rozm�ry znak�. Tabulka je na 26 p�smen, 10 ��sel a 4 n�co, co zbylo (celkem 40 znak�). T�et� Text je Mircosoft�v, p�e jednobarevn�, umo��uje kompletn� znakovou sadu a fonty nahr�v� ze soubor� FNT.</p>

<p>Tu uvedu: Sprajty ve VGA a SVGA a tady u textu WriteText pracuj� tak, �e 1 pixel = 1 bajt, stejn� jako na obrazovce. Pixely s hodnotou nula se pova�uj� za "pr�hlednou" �ernou barvu, kter� se nekresl�. Jak by tedy vypadalo BMP s fontem? Pozad� bude �ern� s barvou rovnou nule a jen pixely jin�ch barev se vykresl�.</p>


<h3>Podknihovna *8* - MAPS</h3>

<p>Mapa je z�klad pro hru, ale i pro jin� ��ely. M�me oblast 640x400 pixel� grafiky. Na monitoru se zobrazuje v��ez nap��klad 100x100 nebo 320x200 a oblast "Mapa" je vyobrazen� segmentu v rozli�en� 320x200 v rozli�en� 640x480 segmenty a pak v 640x480 celek. Mapa je sestaven� z pol��ek 8x8 pixel� (kresl� se p�es Spr nebo Bar) a je ulo�en� jako �ada ��sel 0..499. Tato ��sla jsou indexy pol��ek v tabulce sprit�. Tabulka je ulo�ena v lev� polovin� Bufferu 2 (320x200) ve form� obd�ln�ku 160x200 px, tedy 20x25 pol��ek 8x8 px. Index 0 odpov�d� pol��ku v lev�m horn�m rohu, d�l to pokra�uje doprava a pak po ��dc�ch dol�. Mapa tedy umo��uje z modelov�ch kosti�ek 8x8 v Bufferu 2 sestavovat na obrazovce opakuj�c� se vzory a podobn�. M�lo by to p�ipom�nat dobu Nintendo 8b kazet 128..256 KB.</p>


<h3>Podknihovna *9* - TIME</h3>

<p>Obsahuje Paletu, Speaker a Timer.</p>

<p>��st Timer umo�nuje Delay (�ek�n�), d�le Stopky a prom�nnou Second automaticky blikaj�c� na frekvenci 1 Hz.</p>

<p>��st Paleta umo��uje p�e��st nebo nastavit RGB slo�ky jednotliv�ch barev a vybran� �seky palety asynchronn� stm�vat, rozjas�ovat nebo s nimi blikat. Pracujeme po �estn�ctk�ch (nap�. �sek 16 barev 50..65), odtud i prefix.</p>

<p>��st Speaker p�ehr�v� zvuky na intern�m p�p�tku po��ta�e (PC-Speaker, Dosbox ho emuluje p�es reproduktory zvukov� karty). Je tu definovan� form�t na zvuky: vlna vzork� p�ejede a vyd� to zvuk asi jako kv�knut� nebo zakv�knut�. Pak je tu gener�tor zvuku. Umo��uje vys�lat okt�vu CDEFGAHC tam a zp�t jako houka�ku nebo t�eba pozm�nit frekvence n�kter�ch not a vytv��et fantomov� n�zkofrekven�n� sign�l nav�zan� na hudb� (pou�it� v psychowalkmanu WORLD).</p>


<h3>Podknihovna *10* - SOUND BLASTER 16</h3>

<p>P�ehr�v� zvuky na zvukov� kart�. Jako z�klad byl pou�it Sound Blaster 16 Ethana Brodsk�ho, kter� v�t�inou hraje, a Mircosoftovy Zvuky, kter� nefunguj� v Dosboxu. Ve slo�ce STORE/10-SB16 jsou obsa�en� utility na pr�ci se zvukem.</p>


<h3>Podknihovna *11* - KEYBOARD 1</h3>

<p>Moje Lite kl�vesnice, detekce stisku.</p>


<h3>Podknihovna *12* - KEYBOARD 2</h3>

<p>Mirkosoftova kl�vesnice, komplexn� program, kv�li psan� memo v programu WORLD.</p>


<h3>Podknihovna *13* - MOUSE</h3>

<p>Pr�ce s my��, pohyb, stisk.</p>


<h3>Podknihovna *14* - MATH</h3>

<p>Rutiny pro vyn�en� graf� nebo kreslen� objekt� hry a jejich pohyb po k�ivk�ch. Nap��klad kreslen� s�ely let�c� po dr�ze.</p>


<h3>Podknihovna *15* - IPX</h3>

<p>Zcela funk�n� metoda spojen� dvou dosboxov�ch stanic a v�m�na dat (paket�) po s�ti.</p>


<h3>Podknihovna *16* - MISC</h3>

<p>Na��t�n� a ukl�d�n� datov�ch form�t� pro zvuky, grafiku a mapy. A hlavn� spou�t�c� a ukon�ovac� procedury PowerOn a PowerOff. Ta prvn� se star� o nastartov�n� v�ech ostatn�ch ��st� knihovny (inicializace prom�nn�ch, alokace Buffer� a podobn�), volejte ji hned na za��tku programu. Tu druhou volejte p�ed jeho ukon�en�m, postar� se o �klid a bezpe�n� vypnut�.</p>


<hr>


<h2>Uk�zkov� program WORLD</h2>

<p>Vysoce ��inn� psychedelick� n�stroj navozuj�c� zm�n�n� stavy v�dom�. Od leh��ho om�men� a�? k siln�m stav�m trvaj�c�m dlouho. Postupujte jen s nejvy��� m�rou opatrnosti, pou�ijte nejlep�� rozum, jak� m�te.</p>
