(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)
(*  Soubor: WOKTEXTY.INC                                                   *)
(*  Obsah: indexy textu pro jednotku Woknows 4                             *)
(*  Autor: Mircosoft                                                       *)
(*  Posledni uprava: 13.7.2011                                             *)
(*  Pro kompilaci: nic                                                     *)
(*  Pro spusteni: nic                                                      *)
(*  Upozorneni: tyto zdrojove kody pouzivate na vlastni nebezpeci          *)
(* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *)

const

wtcohelp=1;
wtomalopameti=2;
wtmenuOK=4;
wtdotaz=5;
wtano=6;
wtanohelp=7;
wtne=8;
wtnehelp=9;
wtanoznak=10;
wtneznak=11;
wtzprava=12;
wtok6=13;
wtokhelp=14;
wtokznak=15;
wtzadejtext=16;
wtok4=17;
wtvltoknookhelp=18;
wtzrus=19;
wtvltoknozrushelp=20;
wtzadejcislo=21;
wtvlcoknookhelp=22;
wtvlcoknozrushelp=23;
wtvyberbarvu=24;
wtok10=25;
wtokbarvyhelp=26;
wtzrusbarvuhelp=27;
wtzrusznak=28;
wtjenprocteni=29;
wtskryty=30;
wtsystemovy=31;
wtarchivovat=32;
wtzadne=33;
wtvlastnostisouboru=34;
wtjmeno=35;
wtvelikost=36;
wtposlednizmena=37;
wtatributy=38;
wtdisketa=39;
wtvybersoubor=40;
wtvjednotce=41;
wtnenivlozendisk=42;
wtnovyadr=43;
wtnovyadrhelp=44;
wtinfo=45;
wtinfohelp=46;
wtvsoknookhelp=47;
wtvsoknozrushelp=48;
wtadresar=49;
wtjmenoadresare=50;
wtmaskacary=51;
wtok14=52;
wtvmoknookhelp=53;
wtvmoknozrushelp=54;
wtvysvetlivky=55;
wtvmoknohelp=56;
wtvzorekvyplne=57;
wtvvoknookhelp=58;
wtvvoknozrushelp=59;
wtvvoknohelp=60;
wtnastavenimysi=61;
wtvodorovnacitlivost=62;
wtvelka=63;
wtmala=64;
wtsvislacitlivost=65;
wtkorigovatskakani=66;
wtkorskhelp=67;
wtvymenittlacitka=68;
wtymentlachelp=69;
wtopatrnazkouska=70;
wtopzkouskahelp=71;
wtnmoknozrushelp=72;
wtnastavaleneukladej=73;
wtnmoknonnuhelp=74;
wtnastavauloz=75;
wtnmoknonuhelp=76;
nmoknochybaukl=78;
wtnmoknootazka=79;
wtbksp=80;
wttab=81;
wtenter=82;
wtesc=83;
wtmez=84;
wtins=85;
wtdel=86;
wthome=87;
wtend=88;
wtpgup=89;
wtpgdn=90;
wtkonfiguraceklaves=91;
wtresitkonflikty=92;
wtreskonflhelp=93;
wtzavrit=94;
wtnkoknozavrithelp=95;
wtnkoknohelp=96;
wtpozor=97;
wtnkoknopozor=98;
wtkonzruseno=99;
wtkonchybnezadanecislo=100;
wtkonchybidruhecislo=101;
wtkonchybnedruhecislo=102;
wtkonchybneprvnicislo=103;
wtvymaz=104; {pro vybersouborokno}
wtvymazhelp=105;
wtchybaadr1=106;
wtchybaadr2a=107;
wtchybaadr2b=108;
wtfatalnemalopameti=109;
wtdisky=110;
wtvsonabidka=111;
wtopravduchcetesmazatsoubor=112;
wtvsohelp1=113;
{114 \ taky obsazeno
 115 /          }

{EOF}