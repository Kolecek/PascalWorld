{=== pomocne veci vykopirovane odjinud, aby byl program sobestacny: ===}

function NaStr3(a:longint; NaKolik:byte):string;
var vysl:string[80];
Begin
str(a,vysl);
while length(vysl)<nakolik do vysl:='0'+vysl;
nastr3:=vysl;
End;{nastr3}

const kLatin2='��榛�����ҵ����秜����Ԡ��';
      kWindows1250='�͊�������������횞�����������';

type pole256znaku=array[#0..#255] of char;

procedure PripravPrevodniTabulku(PuvodniZnaky,NoveZnaky:string; var PrevodniTabulka:pole256znaku);
var i,j:byte;
    HledaneZnaky:set of char;
    ch:char;
Begin
hledaneznaky:=[];
for i:=1 to length(puvodniznaky) do include(hledaneznaky,puvodniznaky[i]);
for ch:=#0 to #255 do
 if ch in hledaneznaky then begin
                            j:=1;
                            while puvodniznaky[j]<>ch do inc(j);
                            prevodnitabulka[ch]:=noveznaky[j];
                            end
                       else prevodnitabulka[ch]:=ch;
End;{pripravprevodnitabulku}

function PrekodujRetezec(ktery:string; var PrevodniTabulka:pole256znaku):string;
var i:byte;
    vystup:string;
Begin
vystup[0]:=ktery[0];
for i:=1 to length(ktery) do vystup[i]:=prevodnitabulka[ktery[i]];
prekodujretezec:=vystup;
End;{prekodujretezec}

function ExistujeAdresar(jmeno:pathstr):boolean;
var info:searchrec;
Begin
findfirst(jmeno,anyfile-volumeid,info);
existujeadresar:=(doserror=0)and(info.attr and directory<>0);
End;{existujeadresar}
