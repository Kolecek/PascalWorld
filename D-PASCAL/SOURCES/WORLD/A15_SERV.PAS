{$A-,G+,N+,R-,B-,E-,S-,Q-,V-,X-,F+}

Program a15_Serv;

{XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX}
Uses CRT, DOS,

{LiteX}
x01_glob, x02_ram,  x03_dos,  x04_vga,  x05_svga, x06_3dfx, x07_text, x08_maps,
x09_time, x10_sb16, x11_kbd1, x12_kbd2, x13_mous, x14_math, x15_ipx,  x16_misc,

{World}
a01_Vari, a02_Init, a03_ScrA, a04_ScrB, a05_ScrC, a06_ScrD, a07_Edt1, a08_Edt2,
a09_ProS, a10_ProW, a11_ProW, a12_ProW, a13_Ma_S,

{Inertia 3D engine}
i_Types, i_Polygo, i_Memory, i_BufIO, i_PCX, i_Shade, i_Inerti,

{Ethan Brodsky smix}
b_xms, b_detect, b_smix;


{XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX}
Begin
  PowerOn; VGA_Done;  {server jede v TextModu}
  InitReceive;

  New_HTML;           {uverejnit IPX adresu pro pripojovani jako html soubor}
  ServerMain;

  DoneReceive;
  PowerOff;
End.
