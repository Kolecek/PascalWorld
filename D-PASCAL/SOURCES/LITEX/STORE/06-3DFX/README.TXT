

                        Inertia/16 Source Code Release



  Inertia is continually evolving. As computers get faster and dedicated
hardware becomes more commonplace, advancements must be made. Inertia is no
exception. Starting with Inertia version 1.02, Inertia/16 was no longer
supported. Staring with the next version of Inertia (v2.0), it will be
completely different. All Pascal versions will be dropped and the rendering
architecture will be completely different.


  With this update in mind, I have decided to release the full source code
to Inertia/16 for your enjoyment (or horror).


  This source code is released to the Public Domain. Use at your own risk.


  Use MAKEFILE.TP to build the Turbo Pascal version and MAKEFILE.BP to
build the Borland Pascal (DPMI) version.

  The documentation in the /DOCS directory might be slightly out of date, but
for the most part are accurate.


        Alex Chalfin
        achalfin@one.net



